---
title: 'Valor afegit'
published: true
slug: valor-afegit
---

# Valor afegit 

* LiberaForms és cultura lliure. El seu codi, sota llicència AGPLv3 i disponible en un repositori públic, pot ser instal·lat i auditat per qualsevol persona que tingui els coneixements per fer-ho. I els materials de promoció i formació del projecte estan sota llicència CC BY-SA. 

* A LiberaForms pensem que tothom hauria de tenir dret a les mateixes prestacions d'usuària independentment del seu poder adquisitiu. Per això, totes les persones que usin LiberaForms tindran les mateixes funcionalitats d'usuària. I qui vulgui gaudir de les funcionalitats avançades d'administració i gestió, sempre pot instal·lar la seva pròpia instància de LiberaForms. 

* LiberaForms està dissenyat per respectar els vostres espais i projectes. És per això que els formularis no inclouen marca gràfica ni cap altra propaganda. Així, evitem una agressió publicitària a les persones a qui envieu els formularis. A LiberaForms practiquem el disseny ètic.  
  * [Disseny ètic (en)](https://2017.ind.ie/ethical-design/)

* LiberaForms fa una comunicació comunitària, és a dir, que fa ús de mitjans sobirans i descentralitzats per visibilitzar-se. La web explicativa és un Grav, i no té cap rastrejador ni anunci. Podeu contactar amb l'equip de LiberaFoms a través del correu electrònic info [@] liberaforms.org. També tenim compte de mitjans socials del projecte, que a més de ser visible a la web, s'allotja al node de barcelona.social, part de la federació de mitjans socials lliures FEDIVERSE. LiberaForms va més enllà de les restriccions de la RAP i no fa ús de cap mitjà asocial corporatiu.
  * [Fedivers](https://fedi.cat)
  * [Restriccions de la RAP](https://antipub.org/resistance-a-l-agression-publicitaire-s-infiltre-dans-les-reseaux-sociaux/)

* LiberaForms vol ser sostenible a través de serveis oferts a les persones i entitats que en volen fer un ús personalitzat i/o intensiu. LiberaForms no rep ni vol rebre diners de fons d'inversió ni pretén ser una empresa tipus "start-up".
