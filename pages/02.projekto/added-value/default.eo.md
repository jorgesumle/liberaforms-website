---
title: 'Aldonan valoron'
published: true
slug: aldonan-valoron
---

# Aldonan valoron

* LiberaForms estas liberkulturo. Ĝian kodon, sub licencio AGPLv3 kaj havebla en publika programara deponejo, povas esti instalita pro iu ajn kiu havas la konoj por fari ĝin. Kaj la materialoj de promocio kaj formado de la projeckto estas sub licencio CC BY-SA. 

* En LiberaForms ni pensas ke ĉiuj devas havi rajto a la samaj programaro de la uzanto sendepende de sia aĉetpovo. Pro tio ĉiuj personoj kiu uzas LiberaForms havos la samajn funckionojn de uzanto. Kaj kiu volas uzi altnivelajn funckionojn de administracio ĉiam povas instali propran instaco de LiberaForms.

* LiberaForms estas desegnita por respekti vian ĉiferkunvenejojn kaj projektojn. Pro tio la formaroj ne inkludas grafikan firmaon kaj ne alian propagandon. Tiel evitas reklaman agreson a la personoj kiu vi sendas la formularojn. En LiberaForms ni prakticas la etikan desegnon. 
  * [Etikan desegnon (en)](https://2017.ind.ie/ethical-design/)
  
* LiberaForms faras komunuma komunicado, tio estas ke ĝi uzas suverenajn kaj malcentrajn amaskomunikilojn por videbligi. La klariga retpaĝo estas Grav, kaj ne havas spurilojn ne reklamojn. Vi povas kontakti kun la teamo LiberaForms per retpoŝto info [@] liberaforms.org. Krome vi havas sociajn retojn konton en la projekto, kiu estas videbla en la retpaĝo, kiu gastigas en barcelona.social, parto de la federacio de liberajn sociajn retojn FEDEVERSO. LiberaForms iras preter la limigo de RAP kaj ne uzas neniun korporativan asocian amaskomunikilon.
  * [RAP (fr)](https://antipub.org/resistance-a-l-agression-publicitaire-s-infiltre-dans-les-reseaux-sociaux/)
  * [Fediverso (ca)](https://fedi.cat)

* LiberaForms volas esti daŭrigebla per servoj proponitaj a ĉiuj personoj kaj entoj kiu volas uzin ĝin propre kaj/aŭ intense. LiberaForms ne ricevas kaj ne volas ricevi monon de investaj fondusoj nek pretendas esti entrepreno kiel ekfirmao.

