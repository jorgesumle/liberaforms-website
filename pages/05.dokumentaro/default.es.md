---
title: Docs
published: true
slug: documentacion
---

# Documentación
En este espacio detallamos la documentación para el proyecto LiberaForms.

## Manual de uso
Si quieres empezar a crear formularios con LiberaForms o administrar una instancia, echa un ojo al manual de uso.  
[Manual de uso LiberaForms](https://docs.liberaforms.org)

## Forja
Si quieres crear i mantener tu propia instancia en un servidor, consulta las instrucciones en la forja.  
[Forja LiberaForms](https://gitlab.com/liberaforms)

## Governanza
En LiberaForms hemos acordado un Contrato Social y un Código de Conducta. Si te quieres implicar en el proyecto, tienes que leer y estar de acuerdo con el contenido de estos dos documentos.

* [Contrato Social](https://docs.liberaforms.org/es/socia-kontrakto.html) 
* [Código de Conducta](https://docs.liberaforms.org/es/etiketo.html)

## Otros documentos
Otra documentación pública de LiberaForms, como documentos gráficos, está disponible en la instancia de Arxius Oberts de la associación LaLoka, impulsora del proyecto LiberaForms.  
[Arxius Oberts de LiberaForms](https://arxius.laloka.org/w/projectes/#LiberaForms)
