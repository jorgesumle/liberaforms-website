---
title: Zergatik
published: true
slug: zergatik
routable: false
visible: false
---

# Zergatik
## Testuingurua
Gaur egun, ia espazio eta proiektu guztiek, kolektiboak, elkarteak, kooperatibak edo ikastetxeak diren alde batera utzita, datuak biltzeko beharra dute haien ekintzak burutzeko: erreserbak, kontsultak, menifestuei atxikitzea, etab. Maiz, ezjakintasuna dela eta, enpresa multinazionalen software pribatiboak erabiltzen dira; enpresa hauen negozioa, hain zuzen ere, gure datuak beste enpresei saltzean datza, pertsona bakoitzaren neurrira sortutako publizitatea sor dezaten. 

Datuen bilketari esker GAFAM (Google, Apple, Facebook, Amazon, Microsoft) ek bezalako enpresa multinazionalek badakite nor garen, norekin gauden, zerk arduratzen gaituen, zerk pozten gaitun eta zeintzuk diren gure interes eta planak. Horri guztiari esker, planetako enpresa aberatsenetakoak dira eta aurrekaririk gabeko manipulazio sozialerako ahalmena dute. Baina enpresa handi horien produktu eta zerbitzuak ez dira doakoak: gure datuekin ordaintzen ditugu, nagusi den kapitalismo digitalaren lehengai baitira.

=> [GAFAM - La Quadrature du net (ca)](https://liberaforms.org/ca/perque/gafam-quadrature-cat-a4.pdf)

Bestalde, indarrean dagoen datuak babesteko legeak datuak errespetuz jasotzera behartzen du. Eta gainera, covid-19a geroztik, nahitaezkoa da internet bidez erreserbak egitea kultura-jardueretarako.  

## Etika
Enpresa teknologikoen botere-metaketa areagotu egin da azken urteotan. Hala ere, oraindik ere uste dugu teknologia digitalak aukera ona direla desberdintasunak murriztu, aukerak sortu eta helburu amankomun hori bermatzeko, benetan soziala eta solidarioa den ekonomia batean, kapitalismo harraparitatik urrun. Horregatik sortzen eta hobetzen ditugu ondasun amankomun digitalak, teknologia etikoak kultura askeko testuinguruan.

Eraiki dezagun helburu hori etika digital esplizitu batekin: bota ditzagun GAFAMak gure espazio eta proiektuetatik, erabili dezagun LiberaForms gure galdetegiak sortzeko! 

=> [Erabili dezagun LiberaForms!](https://usem.liberaforms.org/)  
=> [Kontsultatu erabilera baldintzak](https://liberaforms.org/eu/zerbitzuak) 

Edo software libreko beste edozein tresna, hala nola:
* <https://formtools.org> 
* <https://www.drupal.org/project/webform>
* <https://framaforms.org>
* <https://www.limesurvey.org>
* <https://ohmyform.com> 

## LiberaForms
LiberaForms hurbileko kultura librea eta zero-kilometroko softwarea da, AGPLv3 lizentziapean. Programa Lleialtec-en 2019an GNGforms izenarekin garatu bazen ere, ideia bi urte lehenago hasi zen. 2017an, Sants auzoan, l'Hortet de la Farga iraunarazteko borrokan ari zirela, Google Inprimakiak erabiliz sinadurak biltzeko galdetegi bat sortu zen. Eta ez zen datuekin bakarrik ordaintzen: galdetegi kopuru jakin batetik aurrera, ordaindu ezean enpresak datuak bahitzen zizkizun. Horrela sortu zen auzotarrek datuak errespetuz eta ustekaberik gabe jaso ahal izateko galdetegi etiko batzuk sortzeko ideia.  

Hortet-etik eta Lleialtat Santsenca-tik harantzago, LiberaForms bizilagun, entitate eta proiektuen eskura dagoen tresna da. Programa hau bai galdetegi pribatiboak erabiltzen dituzten erabiltzaileek, bai aldez aurretik esperientziarik ez duten beste pertsona batzuek erabiltzeko da. Erraz sortzen, pertsonalizatzen eta kudeatzen ditu galdetegiak, pertsonen datuekin bat dator eta Europako Datuak Babesteko Legea betetzen du. Erabili dezagun LiberaForms gizarte libre, etiko eta subiranoagoa eraikitzeko! :)
