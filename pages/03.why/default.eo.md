---
title: Kial
published: false
slug: kial
---

# Kial
## La kunteksto
Nuntempe, preskaŭ ĉiuj kunvenejoj kaj projektoj, sendepende se ĝi estas kolektivoj, asocioj, kooperativoj aŭ edukado centroj, ĝi havas la bezono de kolekti datumojn per fari vian aktivadojn: rezervoj, konsultoj, aliĝoj al manifestoj, ktp. Ofte, per malkonigo, oni uzas malliberan programaron de multnaciaj korporacioj kiu precize bazi sian entreprenon en la vendo de niaj datumoj al aliaj korporacioj per povi desegni laŭmende reklamojn, specife per ĉiu persono. 

Tra datumojn kolektadon, multnaciaj korporacioj kiel GAFAMiloj (Google, Apple, Facebook, Amazon, Microsoft) konas kiu vi estas, kun kiu vi estas, konas aferojn kiu maltrankviligas nin, nian feliĉojn kaj nian projecktojn. Ĉio ĉi, eblegas ke tiuj korporacioj estas de plej riĉaj en la mondo kaj ke ĝi havas senprecedence socian manipulan potencon. Sed neniu de ĉi tiuj produktoj kaj servoj de ĉi tiuj grandaj korporacioj estas senkosta: ni pagas ĝin kun nian datumojn, kiu estas la krudmaterialo de la ĉifereca reganta kapitalismo.

=> [GAFAMiloj per Komun](https://wiki.komun.org/books/seguridad-y-privacidad/page/kiel-liberigi-nin-de-gafam) 

Aliflanke, la aktuala eŭropa leĝo de protektado de datumoj devigas a la respektema kolektado de datumoj. Krome, pro la kronviruso, êc estas deviga fari rezervojn interrete per partopreni de kulturaj aktivadoj. 

## La etiko
La koncentrado de potenco en teknologiaj korporacioj akrigiĝis dum lastaj tempoj. Malgraŭ tio, ankoraŭ estas personoj kiu pensas ke la ĉiferecaj teknologioj eblas okazo per redukti malegalajn, krei ŝancojn kaj garantii ĉi tiu komunan horizonton en reale socia kaj solidareca ekonomio, malproksime de predantaj kapitalismoj. Pro tio, ni kreas kaj plibonigas komunajn ĉifericajn varojn, etikajn teknologiojn en liberkultura konteksto.

Konstruu kune ĉi tiu horizonton kun eksplicita ĉifereca etiko: forpelu la GAFAM de niaj kunvenejoj kaj projektoj, uzu LiberaForms por krei viajn formularojn!

=> [Provu LiberaForms!](https://usem.liberaforms.org/)  
=> [Konsulti la kondiĉojn de servado](https://liberaforms.org/eo/servoj/)  

Aŭ kelkan alian ilojn de libera programaro kiel: 
* <https://formtools.org>
* <https://www.drupal.org/project/webform>
* <https://framaforms.org>
* <https://www.limesurvey.org>
* <https://ohmyform.com>

## LiberaForms
LiberaForms estas proksima liberkulturo kaj nula kilometro programilo sub licenco AGPLv3. Se ĝi disvolvis en la 2019 jaro per Lleialtec sub la nomo GNGforms, la ideo komencis du jarojn antaŭen. Kiam en la 2017 jaro al kvartalo de Sants (Barcelono, Katalanujo) estis batalanta per konservi lokan urban ĝardenon l'Hortet de la Farga, oni kreis Guglon formularon per kolekti subskribojn. Kaj rezulti ke ne nur oni paĝas kun datumoj: ekde certa nombro da respondojn aŭ oni paĝas aŭ la korporacio sekvestri la datumojn. Kaj tiel naskiĝis la ideon de krei etikajn formularojn do kolektivoj al nia kvartalo povis kolekti datumojn respekteme kaj sensurprize.

Preter l'Hortet kaj la Lleialtat Santsenca, LiberaForms estas ilo usebla per najbaroj kaj kolektivoj kaj projektoj kiu volus uzi ĝin. La programilo estas pensata per esti facila de uzi pro personoj kiu jam uzas malliberajn formularojn kiel pro aliaj personoj kiu ne havas neniu antaŭa sperto. Kun LiberaForms estas tre facila krei, personigi kaj administri formularojn, estas respektema kun datumoj de la homoj kaj plenumi la eŭropa leĝo de protektado de datumoj. Uzu LiberaForms per konstrui socio pli libera, pli etika kaj pli suverana! :)
