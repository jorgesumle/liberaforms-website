---
title: Servicios
published: true
slug: servicios
---

# Servicios
Si los servicios propuestos no se ajustan a vuestro proyecto, contactadnos en el correo info [@] liberaforms.org y explicarnos qué necesitáis para poder hacer una cuota a medida.

![Descripción gráfica de los servicios](liberafoms-modalitats-es.png "Descripción gráfica de los servicios")
Modalidades y condiciones de los servicios que ofrecemos. Precios sin IVA.

## Gratuito
* Sin dominio propio: los formularios serán enviadors con una URL bajo dominio "liberaforms.org".  
Por ejemplo: <https://usem.liberaforms.org/peticion>  
Usa este formulario para crear una usuaria de esta modalidad gratuita.
* Máximo de respuestas anuales: 200. Puedes enviar tantos formularios como quieras pero el máximo de respuestas anuales que podrás recibir será de 200.

Esta opción incluye las funcionalidades de usuaria que son:

* Crear y publicar formularios en línea
* Consultar las respuestas por tabla, gráfico, CSV
* Definir la pantalla de '¡Gracias!', que aparece después de rellenar un formulario
* Recibir notificaciones por correo electrónico
* Incluir un texto propio de Ley de Protección de Datos (LOPD)
* Incluir una casilla para enviar un acuse de recibo
* Definir las condiciones de expiración (fecha, máximo de respuestas, campos numéricos)
* Definir la pantalla de 'Este formulario ha caducado'
* Compartir un enlace para que otras persones puedan consultar respuestas 
* Incluir un enlace para inscrustar el formulario en otra página web
* Consultar tus estadísticas de uso

## Cuota sin dominio propio
* Como en la modalidad gratuita, los formularios serán enviados con una URL bajo dominio "liberaforms.org" y incluye las funcionalidades de usuaria.
* Máximo de respuestas mensuales: 800. Puedes enviar tantos formularios como quieras pero el máximo de respuestas mensuales que podrás recibir será de 800.
* Tarifas: 10€ / mes, 25€ / trimestre, 90€ / año

## Cuota con dominio propio
* Permite emitir formularios bajo otro dominio. Esta opción hace posible el uso de funcionalidades de administración y gestión que detallamos más abajo.
* Máximo de respuestas mensuales: 3000. Puedes enviar tantos formularios como quieras pero el máximo de respuestas mensuales que podrás recibir será de 3000.
* Tarifas: 20€ / mes, 40€ / trimestre, 130€ / año
* Requerimiento extra: instalación del software bajo el dominio pertinente (una sola vez). Precio: 150€ 

Esta opción incluye las funcionalidades de administración y gestión que son:

* Administrar usuarias
* Definir tus términos y condiciones
* Definir y compartir plantillas como la LOPD con las usuarias de tu instancia
* Trabajar en equipo (donde usuarias pueden consultar/usar formularios creados por otras usuarias)
* Crear formularios privados que solo se pueden consultar y modificar si se tiene una cuenta 
* Consultar las estadísticas de uso de la instancia
* Configurar el SMTP y definir la dirección desde donde se envian los correos que genera la aplicación, como "no-reply@tu_dominio.com"
* Personalizar la web poniendo tu marca gráfica
  * Texto de la página de inicio
  * Nombre del sitio web
  * Icono (favicon)
  * Color del menú
