---
title: Partopreni
published: true
slug: partopreni
---

# Partopreni
Estas multaj manieroj partopreni la projekton LiberaForms.

## Uzi kaj disvastigi ĝin
### Uzi ĝin
Uzi la programon estas subteni ĝin, kaj do plibonigi ĝin. Subtenu nin pere de krei kaj diskonigi formularojn per LiberaForms!  
[Provi LiberaForms](https://usem.liberaforms.org/petition)

### Priparoli ĝin
Disvastigi la projekton estas ankaŭ partopreni! Sciigu homojn, grupojn kaj entojn pri la projekto.

### Blogoj
Se vi havas blogon kaj volas dediĉi kelkan tekston, ni volas legi viajn opiniojn. Se vi verkis blogeron, sciigu nin retpoŝte aŭ per la Fediverso.

### Sociaj komunikiloj
Se vi diskonigas per sociaj komunikiloj, vi povas helpi subteni nin per la etikedo #LiberaForms. Kaj se vi volas mencii nin en la Fediverso, ni troviĝas je la nodo barcelona.social.  
[LiberaForms en la Fediverso](https://barcelona.social/users/liberaforms)

## Plibonigi la projekton
### Kodo
Se vi konas programadon kaj trovas erarojn en la kodo aŭ volas proponi pliboniĝon, ni ĝojos se vi sciigos nin per la kodo-deponejo.  
[Kodo-deponejo de LiberaForms](https://gitlab.com/liberaforms)

### Dokumentaro
Se vi trovas ion malĝustan en la dokumentaro aŭ volas sugesti pliboniĝojn, ni ĝojos se vi sciigos nin retpoŝte aŭ per la Fediverso.  
[Dokumentaro de LiberaForms](https://docs.liberaforms.org/)

### Tradukado
Se vi volas ke LiberaForms estu en via lingvo, helpu nin kun la tradukado pere de Weblate.
[Tradukado de LiberaForms](https://hosted.weblate.org/projects/liberaforms/)

## Ekpartopreni
Ekpartopreni la projekton signifas kontribui al ĝia evoluigo, kaj je la infrastrukturo kaj je la enhavo. Se vi volas ekpartopreni en LiberaForms, unue legu la Socian Kontrakton kaj Etiketon.

* [Socia Kontrakto](https://docs.liberaforms.org/eo/socia-kontrakto.html)
* [Etiketo](https://docs.liberaforms.org/eo/etiketo.html)

## Financi ĝin
### Kontrakti servon
Se post provi la programon vi kredas ke vi uzados ĝin pli intense aŭ volas tajlori LiberaForms sub vian domajnon, rigardu la servojn kiujn ni proponas. Kaj se ili ne tauĝas por viaj bezonoj, skribu al ni retpoŝte por tajlori la servon.  
[Servoj de LiberaForms](https://liberaforms.org/eo/servoj)

### Donaci
Vi ankaŭ povas subteni nin per unu-foja aŭ ripeta donaco. LiberaForms estas projekto progresiganta de LaLoka, ne-profitcela asocio kiu progresigas liberkulturon. Donacoj al LiberaForms estas administrataj de la OpenCollective de la LaLoka asocio.  
[Fari donacon](https://opencollective.com/laloka)