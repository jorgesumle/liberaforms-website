---
title: 'Valor añadido'
published: true
slug: mas-valores
---

# Valor añadido

* LiberaForms es cultura libre. Su código, bajo licencia AGPLv3 y disponible en un repositorio público, puede ser instalado y auditado por cualquier persona que tenga los conocimientos para hacerlo. Y los materiales de promoción y formación del proyecto están bajo licencia CC BY-SA. 

* En LiberaForms pensamos que todo el mundo tendría que tener derecho las mismas prestaciones de usuaria independientemente de su poder adquisitivo. Por eso, todas las personas que usan LiberaForms tienen las mismas funcionalidades de usuaria. Y quien quiera disfrutar de las funcionalidades de adminstración y gestión siempre puede instalar su propia instancia de LiberaForms. 

* LiberaForms está diseñado para respetar vuestros espacios y proyectos digitales. Es por eso que los formularios no incluyen marca gráfica ni ninguna otra propaganda. Así, evitamos una agresión publicitaria a las personas a quien mandáis los formularios. En LiberaForms practicamos el diseño ético.
  * [Diseño ético (en)](https://2017.ind.ie/ethical-design/)
  
* LiberaForms hace una comunicación comunitària, es decir, que hace uso de medios soberanos y descentralizados para visibilizar-se. La web explicativa es un Grav, y no tiene ningún rastreador ni anuncio. Podéis contactar con el equipo de LiberaFoms a través del correo electrónico info [@] liberaforms.org. También tenemos cuenta de medios sociales libres que, además de hacerla visible en la web, se aloja en el nodo de barcelona.social, parte de la federación de medios sociales libres FEDIVERSE. LiberaForms va más allá de las restricciones de la RAP y no hace uso de ningún medio asocial corporativo. 
  * [Fediverso (ca)](https://fedi.cat) 
  * [Restriccioness de la RAP (fr)](https://antipub.org/resistance-a-l-agression-publicitaire-s-infiltre-dans-les-reseaux-sociaux/)

* LiberaForms quiere ser sostenible a través de los servicios ofrecidos a las personas y entidades que quieran usarlo de forma personalizada y/o intensiva. LiberaForms no recibe ni quiere recibir dinero que fondos de inversión ni pretende ser una empresa tipo "start-up". 
