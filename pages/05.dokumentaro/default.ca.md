---
title: Documentació
published: true
slug: documentacio
---

# Documentació
En aquest espai detallem la documentació per al projecte LiberaForms.

## Guia d'ús
Si vols començar a crear formularis amb LiberaForms o a administrar una instància, fes una ullada a la guia d'ús.  
[Guia d'ús de LiberaForms](https://docs.liberaforms.org)

## Forja
Si vols crear i mantenir la teva pròpia instància en un servidor, consulta les instruccions a la forja.  
[Forja LiberaForms](https://gitlab.com/liberaforms)

## Governança
A LiberaForms hem acordat un Contracte social i un Codi de conducta. Si et vols implicar al projecte, hauràs de llegir i estar d'acords amb el contingut d'aquests dos documents.

* [Contracte Social](https://docs.liberaforms.org/ca/socia-kontrakto.html) 
* [Codi de Conducta](https://docs.liberaforms.org/ca/etiketo.html)

## Altres documents
Altra documentació pública de LiberaForms, com documents gràfics, està disponible a la instància d'Arxius Oberts de l'associació LaLoka, impulsora del projecte LiberaForms.  
[Arxius Oberts de LiberaForms](https://arxius.laloka.org/w/projectes/#LiberaForms)
