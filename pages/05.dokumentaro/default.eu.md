---
title: Dokumentazioa
published: true
slug: dokumentazioa
---

# Dokumentazioa
Hemen LiberaForms proiekturako dokumentazioa azaltzen dugu.

## Erabilera gida
LiberaForms-ekin galdetegiak sortzen edo instantzia bat kudeatzen hasi nahi baduzu, eman begirada bat erabilera gidari.  
[LiberaForms-en erabilera gida](https://docs.liberaforms.org)

## Kode biltegia
Zure instantzia propioa zerbitzari batean sortu eta mantendu nahi baduzu, begiratu kode biltegiko jarraibideak.  
[LiberaForms-en kode biltegia](https://gitlab.com/liberaforms)

## Gobernantza
LiberaForms-en Kontratu soziala eta Jokabide-kodea adostu ditugu. Proiektuan sartu nahi baduzu, bi dokumentu hauek irakurri eta beren edukiarekin ados egon beharko duzu.


* [Kontratu soziala](https://docs.liberaforms.org/eu/socia-kontrakto.html)
* [Jokabide-kodea](https://docs.liberaforms.org/eu/etiketo.html)

## Bestelako dokumentuak
LiberaForms-en bestelako dokumentazio publikoa, adibidez dokumentu grafikoak, LaLoka elkartearen Arxius Oberts plataforman dago eskuragarri. LaLoka LiberaForms proiektuaren babeslea da.  
[LiberaForms-en Arxius Oberts](https://arxius.laloka.org/w/projectes/#LiberaForms)