---
title: Servoj
media_order: 'liberafoms-modalitats.svg,liberafoms-modalitats-en.png,liberafoms-modalitats-es.png,liberafoms-modalitats-eu.png,liberafoms-modalitats-ca.png,liberafoms-modalitats-eo.png'
published: true
slug: servoj
---

# Servoj
Se la servoj proponitaj ne konvenas vin, kontaktu nin en info [@] liberaforms.org kaj rakontu al ni kion vi bezonas por krei laŭmendan kategorion.

![Grafika Priskribo de la servoj](liberafoms-modalitats-eo.png "Grafika Priskribo de la servoj")
Kategorioj kaj uzokondiĉoj kiujn ni proponas. Kosto sen aldonvalora imposto (AVI).

## Senkosta
* Sen propra domajno: la formularoj estos senditaj kun URL sub "liberaforms.org" domajno.  
Ekzemple: <https://usem.liberaforms.org/petu>  
Uzu ĉi tiun formularon por krei konton kun senkosta kategorio.
* Maksimumo de respondoj po jaro: 200. Vi povos sendi tiom formularojn kiom vi volus sed la maksimumo de respondoj pojare ke vi povos ricevi estos de 200.

Ĉi tiu kategorio eblas funkciojn de uzanto, kiel:

* Krei kaj publiku formularojn en interreto
* Konsulti respondojn per tabulo, grafiko, CSV
* Difini la ekranon de "Dankon!", kiu aperos kiam plenigos formularon
* Ricevi atentigojn per retpoŝto
* Inkludi propran texton pri Leĝo de Protektumado de Datumoj (LOPD)
* Inkludi markobutonon por sendi ricevkonfirmo
* Difini kondiĉojn (limdato, maksimumo da respondoj, nombra kampo)
* Difini la ekranon de "Ĉi tiu formularo malvalidiĝis"
* Kunhavi ligilon por ke aliaj personoj povas konsulti respondojn 
* Inkludi ligilon por enkorpigi la formularon en alia retpaĝo 
* Konsulti viajn statistikojn pri uzado

## Kvoto sen propra domajno
* Kiel la kategorio senkosta, la formularoj estos senditaj sub "liberaforms.org" domajno kaj ĝi inkluzivas funckiojn de uzanto.
* Maksimumo de respondoj pomonate: 800. Vi povos sendi tiom formularojn kiom vi volus sed la respondoj pomonate kiujn vi povos ricevi estos 800.
* Tarifoj: 10€ / monato, 25€ / trimestro, 90€ / jaro.

## Kvoto kun propra domajno
* Eblas krei formularojn sub alia domajno. Ĉi tiu kategorio ebligas la uzon de funkcioj de administracio kaj mastrumado kiujn ni listigis sube.
* Maksimumo de respondoj pomonate: 3000. Vi povos sendi tiom formularojn kiom vi volus sed la respondoj pomonate kiujn vi povos ricevi estos 3000.
* Tarifoj: 20€ / monato, 40€ / trimestro, 130€ / jaro. 
* Ekstra bezono: instalado de la programaro sub propra domajno (unufoje). Tarifo: 150€

Ĉi tiu kategorio ebligas funkciojn de administracio kaj mastrumado kiel:

* Administri uzantojn
* Difini uzokondiĉojn
* Difini kaj kunhavi ŝablonojn pri LOPD kun uzantoj de la apero
* Kunlabori (kie uzantoj povas konsulti/uzi formularon kreataj pro aliaj uzantoj)
* Krei privatajn formularojn kiu sole povas konsulti kaj modifi se vi havas konton 
* Konsulti la statistikoj pri uzado de la apero
* Agordi la SMTP kaj difini la adreson por sendi la retpoŝton de la programilo, kiel "ne-respondu@via_domajno.com"
* Proprigi retpaĝon kun via grafika
  * Ĉefpaĝo-teksto 
  * Retejo-nomo
  * Piktogramon (favicon)
  * Menuokoloro
