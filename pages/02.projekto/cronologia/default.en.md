---
title: Chronology
slug: chronology
---

# Chronology
Here you will find a summary of the chronological evolution of the LiberaForms project.

## 2017
Idea and creation of the software LiberaForms under the name GNGForms (recursive humor: GNGforms is Not Googleforms).

## 2019
We install the first instances of LiberaForms during the project Equipaments Lliures. Entities of the neighborhood of Sants and El Guinardó (Barcelona) begin to make use of them. In addition, the Dutch ecologist entity Code Rood sets up its own instance.

* [Project Equipaments Lliures](https://equipamentslliures.cat/)
* [Project Code Rood](https://code-rood.org)

## 2020
### Other instances
Other fediverse-neighbours set up their instances:

* [Instance of Komun](https://forms.komun.org/)
* [Post on _ voidNull [Spanish]](https://voidnull.es/instalar-liberaforms-en-debian-10-para-olvidarte-de-google-forms/)

### Financing and improvements
We present a proposal to NLnet under the ['Privacy & Trust Enhancing Technologies'](https://nlnet.nl/PET/) project to fund LiberaForms development and they accept it. From November 2020 to October 2021, we have the economic support of this foundation. List of improvements to the project thanks to NLnet support:

* Monitorization
* Database's migration
* Packaging
* Remote Object Storage
* Messaging
* Tables / Data display
* Tests
* Security audit
* Share with RSS
* Form templates
* Accessibility audit
* Governance
* Documentation
* Communication
* Participation

![NLnet logo](https://nlnet.nl/image/logo_nlnet.svg)

### From GNGForms to LiberaForms
A member of the NLnet team makes a comment: "Have you thought that maybe it's not a good idea to include the name of the multinational in your project name? If you want to rename it, now would be a good time." No sooner said than done. In less than half an hour, we came up with the name LiberaForms and we bought the domain.

## 2021
From January to October, we continue developing the project with the support of NLnet Foundation. During the spring, another fedizen sets up their own instance and writes about the installation at esferas.org. Also, the association Sidastudi becomes our first client and, with the incorporation of [Porru](mailto:porru@liberaforms.org) to the team, we set up a new instance of LiberaForms in basque.

* [Post on esferas.org [Spanish]](https://esferas.org/msqlu/2021/05/01/liberaforms-un-servicio-autoalojado-de-formularios-web/)
* [Association Sidastudi](http://www.sidastudi.org/)
* [Basque instance of LiberaForms](https://erabili.liberaforms.org/)