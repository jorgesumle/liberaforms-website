---
title: Project
published: true
slug: project
---

# The project

LiberaForms is free culture and kilometre zero software licensed under the AGPLv3.

* [Added value](/project/added-value)
* [Chronology](chronology)
* [See what it looks like](/project/media)
* [Try LiberaForms](https://usem.liberaforms.org/petition)
* [Contract LiberaForms](/services)


## The beginnings

Software development began in 2019, however the idea came two years earlier. When in 2017 in the neighbourhood of Sants, Barcelona people where organizing to maintain the "Hortet de la Farga" (an urban vegetable garden), they used Google forms to collect signatures of support. And with time they discovered that not only were they paying with their data, but that after a certain number or signatures, the company demanded money, or else. And so the idea was born to build free, ethical form software so that the people of the neighbourhood could collect data respectfully and without surprises.

## Context

In this day and age almost every entity whether they be associations, collectives, cooperatives, NGOs, educational centers ..., need to collect data to carry out their activities: reservations, opinion polls, manifest support, etc. Often, because of a lack of options or understanding, they use multinational proprietary software, software belonging to the same companies who base their business on the sale of our data to third parties, so to design personalized publicity specific to each person.

Through the hording of data these companies, such as the GAFAM (Google, Apple, Facebook, Amazon, Microsoft), know who we are, who we are with, the things that occupy us, our joys, interests and plans. This has made them the richest companies in the world and has given them unprecedented power to manipulate society. But neither their forms nor any of their other products are free: we pay with our data, the raw material of digital capitalism.

[GAFAM by La Quadrature du net](gafam-quadrature-cat-a4.pdf) (català)

Also, let's not forget that data protection law obliges us to collect data respectfully.

## Ethics

The concentration of power in these tech companies has only grown over the last few decades. Even so, there are still people who believe that digital technologies can be used to lower inequality, create opportunities, and build a sustainable society. This is our horizon and it is for these reasons that we develop digital commons and ethical technologies in the context of free culture.

Let's build this common horizon together by practicing explicit digital ethics: throw the GAFAM out of our spaces and projects, use LiberaForms!

## Other free software

We are not the only project that offers form software. If LiberaForms does not meet your needs, one of these other free form software projects might interest you:

* [https://formtools.org](https://formtools.org)
* [https://www.drupal.org/project/webform](https://www.drupal.org/project/webform)
* [https://yakforms.org/](https://yakforms.org/)
* [https://www.limesurvey.org](https://www.limesurvey.org)
* [https://ohmyform.com](https://ohmyform.com)
