---
title: Proiektua
published: true
slug: proiektua
---

# Proiektua
LiberaForms hurbileko kultura librea eta zero-kilometroko softwarea da, AGPLv3 lizentziapean.

* [Balio erantsia](balio-erantsia)
* [Kronologia](kronologia)
* [Bota begirada bat](media)
* [Probatu LiberaForms](https://erabili.liberaforms.org/eskaera)  
* [Kontratatu zerbitzu bat](/zerbitzuak)

## Jatorria
Softwarea 2019. urtean hasi zen garatzen GNGForms izenarekin, baina ideia bi urte lehenago sortu zen. 2017an, Sants auzoan, l'Hortet de la Farga iraunarazteko borrokan ari zirela, Google Inprimakiak erabiliz sinadurak biltzeko galdetegi bat sortu zuten. Eta ez zen datuekin bakarrik ordaintzen: galdetegian jasotako erantzun kopuru jakin batetik aurrera, ordaindu ezean enpresak datuak bahitzen zizkizun. Horrela sortu zen auzoko kolektiboek datuak errespetuz eta ustekaberik gabe jaso ahal izateko galdetegi etiko batzuk sortzeko ideia.

## Testuingurua
Gaur egun, ia espazio eta proiektu guztiek, kolektiboak, elkarteak, kooperatibak edo ikastetxeak diren alde batera utzita, datuak biltzeko beharra dute haien ekintzak burutzeko: erreserbak, kontsultak, manifestuei atxikitzea, etab. Maiz, ezjakintasuna dela eta, enpresa multinazionalen software pribatiboak erabiltzen dira; enpresa hauen negozioa, hain zuzen ere, gure datuak beste enpresei saltzean datza, pertsona bakoitzaren neurrira sortutako publizitatea sor dezaten. 

Datuen bilketari esker, GAFAM (Google, Apple, Facebook, Amazon, Microsoft) bezalako enpresa multinazionalek badakite nor garen, norekin gauden, zerk arduratzen gaituen, zerk pozten gaitun eta zeintzuk diren gure interes eta planak. Horri guztiari esker, planetako enpresa aberatsenetakoak dira eta aurrekaririk gabeko manipulazio sozialerako ahalmena dute. Baina enpresa handi horien produktu eta zerbitzuak ez dira doakoak: gure datuekin ordaintzen ditugu, nagusi den kapitalismo digitalaren lehengai baitira.

=> [GAFAM - La Quadrature du net (ca)](https://liberaforms.org/ca/perque/gafam-quadrature-cat-a4.pdf)

Bestalde, indarrean dagoen datuak babesteko legeak datuak errespetuz jasotzera behartzen du. Eta gainera, Covid-19a geroztik, nahitaezkoa da internet bidez erreserbak egitea kultura-jardueretarako.  

## Etika
Enpresa teknologikoen botere-metaketa areagotu egin da azken urteotan. Hala ere, oraindik ere uste dugu teknologia digitalak aukera ona direla desberdintasunak murriztu, aukerak sortu eta helburu amankomun hori bermatzeko, benetan soziala eta solidarioa den ekonomia batean, kapitalismo harraparitatik urrun. Horregatik sortzen eta hobetzen ditugu ondasun amankomun digitalak, teknologia etikoak kultura askearen testuinguruan.

Eraiki dezagun helburu hori etika digital zehatz batekin: bota ditzagun GAFAMak gure espazio eta proiektuetatik, erabili dezagun LiberaForms gure galdetegiak sortzeko! 

## Beste tresnak
Ez gara galdetegiak eskaintzen dituen proiektu bakarra. LiberaForms zure beharretara egokitzen ez bada, agian beste pertsona batzuek sortu dituzten tresna libreei begiratu beharko diezu:

* <https://formtools.org>
* <https://www.drupal.org/project/webform>
* <https://yakforms.org/>
* <https://www.limesurvey.org>
* <https://ohmyform.com>
