---
title: Docs
published: true
slug: documentation
---

# Documentation
Here we list the different documentation available for the LiberaForms project.

## Users' guide
If you want to start creating forms with LiberaForms or administering an instance, take a look at the users' guide.  
[LiberaForms Users' guide](https://docs.liberaforms.org)

## Blog
The main purpose of the blog is to post an article for each new release of LiberaForms, so we'll be referencing it as a source of upgrade information.  
We also expect to publish the occasional article about LiberaForms in general.  
[LiberaForms Blog](https://blog.liberaforms.org)

## Code repository
If you want to create and maintain your own instance on a server, check the instructions on the code repository.  
[Code repository of LiberaForms](https://gitlab.com/liberaforms)

## Governance
At LiberaForms, we have agreed on a Social Contract and a Code of Conduct. If you want to get involved in the project, you will have to read and agree on the content of these two documents.

* [Social Contract](https://docs.liberaforms.org/en/socia-kontrakto.html)
* [Code of Conduct](https://docs.liberaforms.org/en/etiketo.html)

## Other documents
Other public documentation of LiberaForms, such as graphic documents, is available in the Arxius Oberts instance of the association LaLoka, the promoter of the LiberaForms project.  
[Arxius Oberts of LiberaForms](https://arxius.laloka.org/w/projectes/#LiberaForms)