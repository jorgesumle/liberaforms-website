---
title: Participate
published: true
slug: participate
---

# Participate
There are many ways to participate in the LiberaForms project.

## Use it, spread the word

### Use it
As more people use LiberaForms more improvments will be made. Support us by creating and sharing forms with LiberaForms!</br>
[Try LiberaForms](https://usem.liberaforms.org/petition)

### Talk about it
Talking about the project is a good way to participate! Tell people, groups and entities so they know about it too. 

### Blogs
If you have a blog and you want to dedicate some lines, we'll like to read your impressions. If you have written something already, let us know by email or on the Fediverse.

### Social media
On social media you can help us up by using the hashtag #LiberaForms. If you want to mention us on the Fediverse, we're at barcelona.social.</br>
[LiberaForms on the Fediverse](https://barcelona.social/users/liberaforms)

## Improve the project

### Code
If you are a person familiar with software development and find some errors in the code or want to propose an improvement, we will be happy if you open an issue.</br>
[LiberaForms' code repository](https://gitlab.com/liberaforms)

### Documentation
If you see anything that needs correcting or want to propose improvements, we will be glad if you let us know via e-mail or on the Fediverse.</br>
[LiberaForms' documentation](https://docs.liberaforms.org/)

### Translations
We love tranlsations! If you would like to use LiberaForms in your language, please help us. We use Weblate.  
[LiberaForms Translations](https://hosted.weblate.org/projects/liberaforms/)


## Get involved
Becoming involved in the project means contributing to its development, both in the infrastructure and in the content. If you want to get involved in LiberaForms, read the Social Contract and the Code of Conduct first.

* [Social Contract](https://docs.liberaforms.org/en/socia-kontrakto.html)
* [Code of Conduct](https://docs.liberaforms.org/en/etiketo.html)

## Fund it

### Contract a service
If after trying LiberaForms you would like to use it more frequently or customize LiberaForms under your own domain and manage your own users, take a look at the services we offer. If they don't fit your needs, write us an email and we can come to an arrangement.</br>
[Services of LiberaForms](https://liberaforms.org/en/services)

### Donate
You can also support us by making a single or recurring donation. LiberaForms is a project promoted by LaLoka, a non-profit association that promotes free culture. Donations to LiberaForms are managed through the OpenCollective account of the LaLoka association.</br>
[Make a donation](https://opencollective.com/laloka)