---
title: Serveis
media_order: 'liberafoms-modalitats-en.png,liberafoms-modalitats-es.png,liberafoms-modalitats-eu.png,liberafoms-modalitats-eo.png,liberafoms-modalitats-ca.png,liberafoms-modalitats-eu.svg,liberafoms-modalitats-en.svg,liberafoms-modalitats-es.svg,liberafoms-modalitats-eo.svg,liberafoms-modalitats-ca.svg'
published: true
slug: serveis
---

# Serveis
Si els serveis proposats no s'ajusten al vostre projecte, contacteu-nos a info [@] liberaforms.org i expliqueu-nos què necessiteu per poder fer una quota a mida.

![Descripció gràfica dels serveis](liberafoms-modalitats-ca.png "Descripció gràfica dels serveis") 
Modalitats i condicions dels serveis que oferim. Preus sense IVA.

## Gratuït
* Sense domini propi: els formularis seran enviats amb una URL sota domini "liberaforms.org".  
Per exemple: <https://usem.liberaforms.org/peticio>  
Fes servir aquest formulari per a fer-te una usuària amb aquesta modalitat gratuïta.
* Màxim de respostes anuals: 200. Pots enviar tants formularis com vulguis però el màxim de respostes anuals que podràs rebre serà de 200.

Aquesta opció inclou les funcionalitats d'usuària, que són:

* Crear i publicar formularis en línia
* Consultar, crear i descarregar les respostes per taula, gràfic i CSV
* Definir la pantalla de 'Gràcies!', que apareix un cop s'omple un formulari
* Rebre notificacions per correu electrònic
* Incloure un text propi de Llei de Protecció de Dades (LOPD)
* Incloure una casella per enviar un justificant de recepció
* Definir les condicions d'expiració (data, màxim de respostes, camps numèrics)
* Definir la pantalla de 'Aquest formulari ha caducat'
* Compartir un enllaç perquè altres puguin consultar respostes  
* Incloure un enllaç per a inscrustar el formulari a una altra web
* Consultar les teves estadístiques d'ús

## Quota sense domini propi
* Com a la modalitat gratuïta, els formularis seran enviats amb una URL sota domini "liberaforms.org" i inclou les funcionalitats d'usuària.
* Màxim de respostes mensuals: 800. Pots enviar tants formularis com vulguis però el màxim de respostes mensuals que podràs rebre serà de 800. 
* Tarifes: 10€ / mes, 25€ / trimestre, 90€ / any. 

## Quota amb domini propi
* Permet emetre formularis sota el teu domini o subdomini. Aquesta opció fa possible l'ús de funcionalitats d'administració i gestió que llistem més avall.
* Màxim de respostes mensuals: 3000. Pots enviar tants formularis com vulguis però el màxim de respostes mensuals que podràs rebre serà de 3000.
* Tarifes: 20€ / mes, 40€ / trimestre, 130€ / any
* Requeriment extra: instal·lació del programari sota el domini pertinent (un sol cop). Preu: 150€ 

Aquesta opció inclou les funcionalitats d'administració i gestió, que són:

* Administrar usuàries
* Definir els teus termes i condicions 
* Definir i compartir plantilles LOPD amb les usuàries de la teva instància 
* Treballar en equip (on usuàries poden consultar/usar formularis creats per altres usuàries)
* Crear formularis privats que només es poden consultar i modificar si es té un compte 
* Consultar les estadístiques d'ús de la instància
* Configurar el SMTP i definir la direcció des d'on s'envien els correus que genera l'aplicació, com "no-reply@el_teu_domini.com"
* Personalitzar la web posant-hi la teva marca gràfica
  * Text de la pàgina d'inici
  * Nom del lloc web
  * Icona (favicon)
  * Color del menú
