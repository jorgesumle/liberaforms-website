---
title: Dokumentaro
published: true
slug: dokumentaro
---

# Dokumentaro
Ĉi tie ni klarigas la dokumentaron por la projekto LiberaForms.

## Uzogvidilo
Se vi volas komenci krei formularojn per LiberaForms aŭ administri aperon, konsultu la uzogvidilon.  
[Uzogvidilo de LiberaForms](https://docs.liberaforms.org)

## Deponejo
Se vi volas krei kaj funkciteni vian propran aperon sur servilo, rigardu la instrukciojn en la deponejo.  
[Deponejo de LiberaForms](https://gitlab.com/liberaforms)

## Regado
En LiberaForms, ni interkonsentis pri Socia Kontrakto kaj Etiketo. Se vi volas partopreni la projekton, necesas legi kaj konsenti pri la enhavo de ĉi tiuj du dokumentoj.

* [Socia Kontrakto](https://docs.liberaforms.org/eo/socia-kontrakto.html)
* [Etiketo](https://docs.liberaforms.org/eo/etiketo.html)

## Aliaj dokumentoj
Aliaj publikaj dokumentoj de LiberaForms, ekzemple grafikaj dokumentoj, estas akireblaj en la Arxius Oberts apero de la asocio LaLoka, la progresiganto de la projekto LiberaForms.  
[Arxius Oberts de LiberaForms](https://arxius.laloka.org/w/projectes/#LiberaForms)