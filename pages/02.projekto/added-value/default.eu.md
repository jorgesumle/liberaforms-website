---
title: 'Balio erantsia'
published: true
slug: balio-erantsia
---

# Balio erantsia

* LiberaForms kultura librea da. Bere kodea, AGPLv3 lizentziapean eta biltegi publiko batean eskuragarri, horretarako ezagutzak dituen edozein pertsonak instalatu eta ikuskatu dezake. Eta proiektua sustatu eta hobetzeko materialak CC BY-SA lizentziapean daude.

* LiberaForms-en uste dugu denek eskubide berberak izan beharko genituzkela erabiltzaile gisa, bakoitzaren erosteko ahalmena edozein dela ere. Horregatik, LiberaForms erabiltzen duten pertsona guztiek erabiltzailearen funtzionalitate berak dituzte. Eta administrazioaren eta kudeaketaren funtzionalitateez gozatu nahi duenak beti jar dezake bere LiberaForms instantzia propioa.

* LiberaForms zuen espazio eta proiektu digitalak errespetatzeko diseinatuta dago. Horregatik, galdetegietan ez dago ez marka grafikorik ez bestelako propagandarik. Horrela, galdetegiak bidaltzen dizkiezuen pertsonek publizitate erasorik jasotzea ekiditen dugu. LiberaForms-en diseinu etikoa praktikatzen dugu.  

* LiberaForms-ek komunikazio komunitarioa egiten du, hau da, baliabide subiranoak eta deszentralizatuak erabiltzen ditu bere burua ikusarazteko. Azalpenerako webgunea Grav bat da eta ez du ez aztarnaririk ez iragarkirik. LiberaForms-eko taldearekin harremanetan jar zaitezkete info [@] liberaforms.org e-posta helbidearen bidez. Sare sozial libreetan kontuak ditugu ere, webgunean ikusarazteaz gain, barcelona.social nodoan ostatatzen dena, FEDIBERTSOA deituriko sare sozial libreen federazioaren parte dena. LiberaForms RAParen murrizketak baino haratago doa eta ez du sare asozial korporatiborik erabiltzen.

* LiberaForms-ek modu pertsonalizatuan edota intentsiboan erabili nahi duten pertsonei eta erakundeei eskaintzen zaizkien zerbitzuen bidez jasangarria izateko asmoa du. LiberaForms-ek ez du jasotzen eta ez du jaso nahi inbertsio-funtsetako dirurik, ezta "start-up" motako enpresa izan nahi ere.
