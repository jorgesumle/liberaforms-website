---
title: Hasiera
published: true
slug: hasiera
---

# Galdetegi etikoak LiberaForms-ekin
LiberaForms software libreko tresna bat da, azpiegitura komunitario, libre eta etiko gisa pentsatu eta garatua, erabiltzen duten pertsonen eskubide digitalak errespetatzen dituzten galdetegiak sortu eta kudeatzeko aukera ematen duena.

LiberaForms-ekin zure galdetegietan jasotako erantzunak kontsultatu, editatu eta deskargatu ditzakezu; Datu Pertsonalen Babeserako Legea onartzeko kontrol-lauki bat gehitu; beste erabiltzaile batzuekin elkarlanean aritu zaitezke, behin baimenak partekatuta; eta askoz gauza gehiago egin ahal izango dituzu!

Ezagutu hobeto [proiektua!](/proiektua)

LiberaForms AGPLv3 lizentziapeko kultura librea da. Erabili, partekatu eta hobetu!

* Bota [begirada bat](/proiektua/media)
* LiberaForms probatzeko, [osatu galdetegi hau](https://erabili.liberaforms.org/eskaera)
* Zure LiberaForms propioa instalatzeko, [begiratu iturburu-kodea](https://gitlab.com/liberaforms/liberaforms)
