---
title: 'Parte hartu'
published: true
slug: parte-hartu
---

# Parte hartu
LiberaForms proiektuan parte hartzeko modu ugari dago.

## Erabili eta zabaldu
### Erabili ezazu
Programa erabiltzea berbera indartzea da, eta hortaz, hobetzea. Lagundu gaitzazu LiberaForms-ekin galdetegiak sortuz eta partekatuz!  
[Probatu LiberaForms](https://erabili.liberaforms.org/eskaera)

### Ahoz aho zabaldu
Proiektua zabaltzea ere parte hartzea da! Hitz egin jendearekin, taldeekin eta erakundeekin proiektua ezagutu dezaten.

### Blogak
Blog bat badaukazu eta lerro batzuk eskaini nahi badizkiguzu, zure iritziak gustura irakurriko ditugu. Dagoeneko bidalketa bat idatzi baduzu, esaguzu epostaz edo Fedibertsotik.

### Sare sozial digitalak
Sare sozial digitalen bidez zabaltzen baduzu, #LiberaForms etiketa erabiliz bat egiten lagundu gaitzakezu. Eta Fedibertsoan aipatu nahi bagaituzu, barcelona.social nodoan gaude.  
[LiberaForms Fedibertsoan](https://barcelona.social/users/liberaforms)

## Hobetu proiektua
### Kodea
Softwarearen garapena ezagutzen baduzu eta kodean akatsik aurkitzen baduzu edo hobekuntzarik proposatu nahi baduzu, kode-biltegian esatea eskertuko dizugu.  
[LiberaForms-en kode-biltegia](https://gitlab.com/liberaforms)

### Dokumentazioa
Dokumentazioan akatsik topatzen baduzu edo hobekuntzak proposatu nahi badituzu, Fedibertsotik edo epostaz esatea eskertuko dizugu.  
[LiberaForms-en dokumentazioa](https://docs.liberaforms.org/)

### Itzulpenak
LiberaForms zure hizkuntzan egotea nahi baduzu, lagundu gaitzazu itzulpenekin Weblate bidez.
[LiberaForms itzulpenak](https://hosted.weblate.org/projects/liberaforms/)


## Sartu proiektuan
Proiektuan sartzeak bere garapenean parte hartzea esan nahi du, hala azpiegituran, nola edukian. LiberaForms-en sartu nahi baduzu, irakurri Kontratu soziala eta Jokabide-kodea lehenengo.

* [Kontratu soziala](https://docs.liberaforms.org/eu/socia-kontrakto.html)
* [Jokabide-kodea](https://docs.liberaforms.org/eu/etiketo.html)

## Finantzatu ezazu
### Kontratatu zerbitzu bat
Programa probatu ostean erabilera intentsiboagoa emango diozula uste baduzu edo zure domeinupean LiberaForms pertsonalizatu nahi baduzu, eman begirada bat proposatzen ditugun zerbitzuei. Eta ez badute behar duzuna asetzen, idatziguzu eposta mezu bat zerbitzua pertsonalizatzeko.  
[LiberaForms-en zerbitzuak](https://liberaforms.org/eu/zerbitzuak)

### Egin dohaintza
Noizean behineko edo dohaintza errepikari bat egiten ere lagundu gaitzakezu. LiberaForms LaLoka-k babestutako proiektu bat da, kultura librea sustatzen duen irabazi asmorik gabeko elkartea. LiberaForms-entzako dohaintzak LaLoka elkartearen OpenCollective plataformaren bitartez kudeatzen dira.  
[Egin dohaintza](https://opencollective.com/laloka)

