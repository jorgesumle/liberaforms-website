---
title: Home
published: true
slug: home
---

# LiberaForms, ethical form software

LiberaForms is a libre software tool developed as community, free, and ethical infrastructure that makes it easy to create and manage forms that respect the digital rights of the people who use it.

With LiberaForms you can browse, edit, and download the answers to your forms; include a checkbox to require Data protection law consent, collaborate with other users by sharing permissions; and much more!

Learn more about [the project](/project)

LiberaForms is Libre culture published under the AGPLv3 license: use it, share it, improve it!

* See what it [looks like](/project/media)
* If you want to try out LiberaForms, [fill out this form](https://usem.liberaforms.org/petition)
* If you want to install your own LiberaForms, check out [the code](https://gitlab.com/liberaforms)
